all: libkafel.so

kafel/libkafel.a: kafel/Makefile
	${MAKE} -C kafel

clean:
	${MAKE} -C kafel clean
	${RM} libkafel.so

libkafel.so: src/kafel.cpp kafel/libkafel.a
	${CXX} -o $@ src/kafel.cpp -std=c++20 \
		$$(pkg-config --cflags --libs-only-L --libs-only-other emilua) \
		-fvisibility=hidden -Ikafel/include \
		-Wl,--as-needed -Wl,--allow-shlib-undefined -shared -fPIC \
		-Wl,--start-group $$(pkg-config --libs-only-l emilua) \
		kafel/libkafel.a -Wl,--end-group \
		${CXXFLAGS} ${CPPFLAGS} ${LDFLAGS}

.PHONY: all clean
