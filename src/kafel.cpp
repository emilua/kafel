#include <emilua/native_module.hpp>
#include <emilua/byte_span.hpp>

#include <sys/syscall.h>
#include <kafel.h>

#include <boost/preprocessor/stringize.hpp>
#include <boost/scope_exit.hpp>

static constexpr std::string_view DEFAULT_POLICIES = R"kafel(
// These policies are heavily influenced by Docker's default profile. Further
// customization done on top:
//
// - Avoid syscalls that need root anyway. The policies here are mostly meant to
//   be used by unprivileged users (not containers with root inside). The
//   syscalls wouldn't be harmful, but would result in larger BPF programs that
//   in turn incur more overhead.
// - Avoid rarely used syscalls that can be abused for yet more fingerprinting
//   on desktop applications. This category mostly contains syscalls useful for
//   profiling (e.g. mincore, cachestat).
// - Split them into categories inspired by systemD's seccomp filter sets and
//   OpenBSD's pledge promises.

POLICY Aio {
    ALLOW {
        io_cancel, io_destroy, io_getevents, io_pgetevents, io_setup, io_submit
    }
}

POLICY BasicIo {
    ALLOW {
        read, readv, tee, vmsplice, write, writev,

        // ioctl() is definitively not about generic/stream/basic I/O. ioctl()
        // is really a syscall in disguise that device drivers can use for
        // anything. However it's expected that any program doing file I/O or
        // socket I/O or TTY IO will eventually stumble on glibc using ioctl()
        // for some operations so let's go ahead and just include it in the
        // basic IO set to force other IO categories to include it too.
        ioctl
    }
}

POLICY Clock {
    ALLOW {
        clock_getres, clock_gettime, gettimeofday, time, times
    }
}

// Compat quirks. This family of policies is a good candidate to be maintained
// in a different repo.
POLICY CompatX86 {
    ALLOW {
        // important for old ABI emulation
        personality(persona) {
            persona == /*PER_LINUX=*/0 || persona == /*PER_LINUX32=*/8 ||
            persona == /*UNAME26=*/0x0020000 ||
            persona == /*PER_LINUX32|UNAME26=*/0x20008 ||
            persona == 0xffffffff
        },

        // Important for x86 family's ABI. We put it in here instead of
        // c-runtime because other archs don't need it. Ideally kafel would
        // allow us to write arch_prctl@amd64 in c-runtime and the rule would
        // only be included when we're building for the amd64 arch.
        arch_prctl
    }
}

POLICY CompatDB32 {
    ALLOW {
        remap_file_pages
    }
}

POLICY CompatSystemd {
    ALLOW {
        // SystemD uses this to get mount-id
        name_to_handle_at
    }
}

POLICY CompatWine {
    ALLOW {
        modify_ldt
    }
}

POLICY Credentials {
    ALLOW {
        getegid, geteuid, getgid, getgroups, getresgid, getresuid, getuid
    }
}

POLICY CredentialsExtra {
    ALLOW {
        // SystemD lists this syscall in the policy 'process' with the reasoning
        // that it's able to query arbitrary processes so it's a process
        // relationship related syscall. Following the same reasoning, we opt to
        // not include this syscall in the policy 'credentials' as other
        // syscalls in that category don't allow querying arbitrary
        // processes. However we also opt to not include capget in the category
        // 'process' given most usages of that policy won't need capget at all
        // and would just make the resulting BPF bigger.
        capget
    }
}

POLICY CredentialsMutation {
    ALLOW {
        capset, setfsgid, setfsuid, setgid, setgroups, setregid, setresgid,
        setresuid, setreuid, setuid
    }
}

// Memory allocation, threading, syscall interaction (or libc support) and
// functions that should always be available (e.g. exit_group to bail out as a
// program's last resort).
//
// Do notice that actually opening a libc-based program requires access to much
// more syscalls as the loader is going to scrape the filesystem for the
// required libraries and do many operations to stich the program image
// together. The idea here is to apply a filter that will allow the C runtime to
// keep running after we already have the program image in RAM.
POLICY CRuntime {
    ALLOW {
        brk, exit, exit_group, futex, futex_requeue, futex_wait, futex_waitv,
        futex_wake, get_robust_list, get_thread_area, gettid, madvise,
        map_shadow_stack, membarrier, mmap, mprotect, mremap, munmap,
        restart_syscall, rseq, sched_yield, set_robust_list, set_thread_area,
        set_tid_address,

        // glibc's malloc() has references to getrandom(), so it's included here
        getrandom
    }
}

// These syscalls are already gated by YAMA's ptrace_scope or capabilities
// (e.g. CAP_PERFMON). The usual reasoning would be that it's safe to permit
// them, but:
//
// - They are really only useful for process inspection/debugging.
// - For IPC usage, better mechanisms exist (e.g. one can memfd+seal+mmap to
//   have zero copy I/O between cooperating processes).
// - They appeared in a few CVEs in the past.
POLICY Debug {
    ALLOW {
        kcmp, pidfd_getfd, perf_event_open, process_madvise, process_mrelease,
        process_vm_readv, process_vm_writev, ptrace
    }
}

POLICY FileDescriptors {
    ALLOW {
        close, close_range, dup, dup2, dup3, fcntl
    }
}

// This policy is split off from filesystem so a process could still perform
// file IO on:
//
// - Already open files.
// - Files received from UNIX sockets.
// - Memfds.
POLICY FileIo {
    ALLOW {
        copy_file_range, fadvise64, fallocate, flock, ftruncate, lseek, pread64,
        preadv, preadv2, pwrite64, pwritev, pwritev2, readahead, sendfile,
        splice
    }
}

// OpenBSD's pledge further breaks down this promise into rpath, wpath, cpath
// and dpath, but Landlock would be more appropriate to mirror the intention of
// such granular designs
POLICY Filesystem {
    ALLOW {
        access, chdir, creat, faccessat, faccessat2, fchdir, fgetxattr,
        flistxattr, fstat, fstatfs, getcwd, getdents, getdents64, getxattr,
        inotify_add_watch, inotify_init, inotify_init1, inotify_rm_watch,
        lgetxattr, link, linkat, listxattr, llistxattr, lstat, mkdir, mkdirat,
        mknod, mknodat, newfstatat, open, openat, openat2, readlink, readlinkat,
        rename, renameat, renameat2, rmdir, stat, statfs, statx, symlink,
        symlinkat, truncate, umask, unlink, unlinkat
    }
}

// Allowed to make explicit changes to fields in struct stat relating to a file.
POLICY FilesystemAttr {
    ALLOW {
        chmod, chown, fchmod, fchmodat, fchmodat2, fchown, fchownat,
        fremovexattr, fsetxattr, futimesat, lchown, lremovexattr, lsetxattr,
        removexattr, setxattr, utime, utimensat, utimes
    }
}

// Event loop system calls.
POLICY IoEvent {
    ALLOW {
        epoll_create, epoll_create1, epoll_ctl, epoll_ctl_old, epoll_pwait,
        epoll_pwait2, epoll_wait, epoll_wait_old, eventfd, eventfd2, poll,
        ppoll, pselect6, select
    }
}

// io_uring nowadays is considered unsafe for general usage:
// http://security.googleblog.com/2023/06/learnings-from-kctf-vrps-42-linux.html
POLICY IoUring {
    ALLOW {
        io_uring_enter, io_uring_register, io_uring_setup
    }
}

// SysV IPC, POSIX Message Queues or other IPC.
POLICY Ipc {
    ALLOW {
        memfd_create, mq_getsetattr, mq_notify, mq_open, mq_timedreceive,
        mq_timedsend, mq_unlink, msgctl, msgget, msgrcv, msgsnd, pipe, pipe2,
        semctl, semget, semop, semtimedop, shmat, shmctl, shmdt, shmget
    }
}

// Memory locking control.
POLICY Memlock {
    ALLOW {
        memfd_secret, mlock, mlock2, mlockall, munlock, munlockall
    }
}

POLICY NetworkIo {
    ALLOW {
        connect, getpeername, getsockname, getsockopt, recvfrom, recvmmsg,
        recvmsg, sendmmsg, sendmsg, sendto, setsockopt, shutdown
    }
}

POLICY NetworkServer {
    ALLOW {
        accept, accept4, bind, listen
    }
}

POLICY NetworkSocketTcp {
    ALLOW {
        socket(domain, type, protocol) {
            (domain == /*AF_INET=*/2 || domain == /*AF_INET6=*/10) &&
            (type & 0x7ff) == /*SOCK_STREAM=*/1 &&
            (protocol == 0 || protocol == /*IPPROTO_TCP=*/6)
        }
    }
}

POLICY NetworkSocketUdp {
    ALLOW {
        socket(domain, type, protocol) {
            (domain == /*AF_INET=*/2 || domain == /*AF_INET6=*/10) &&
            (type & 0x7ff) == /*SOCK_DGRAM=*/2 &&
            (protocol == 0 || protocol == /*IPPROTO_UDP=*/17)
        }
    }
}

POLICY NetworkSocketUnix {
    ALLOW {
        socket(domain, type, protocol) {
            domain == /*AF_UNIX=*/1 && protocol == 0
        },
        socketpair(domain, type, protocol) {
            domain == /*AF_UNIX=*/1 && protocol == 0
        }
    }
}

// System calls used for memory protection keys.
POLICY Pkey {
    ALLOW {
        pkey_alloc, pkey_free, pkey_mprotect
    }
}

// Process control, execution, namespacing, relationship operations.
//
// Most likely you'll ALWAYS need access to this set to sandbox other binaries:
// <https://lore.kernel.org/all/202010281500.855B950FE@keescook/T/>. It's only
// really practical to exclude this set from the seccomp filter if you're
// sandboxing yourself (i.e. cooperatively dropping further privileges before
// doing dangerous stuff). It's a shame that Linux doesn't offer this type of
// transition-on-exec mechanism for seccomp nor cgroups. Folks from SELinux
// already know just how important it is to support this kind of mechanism for
// properly dropping privileges, and it'd be good for more kernel hackers to
// learn this lesson as well.
POLICY Process {
    ALLOW {
        // Where's clone2? ia64 is the only architecture that has clone2, but
        // ia64 doesn't implement seccomp. c.f.
        // acce2f71779c54086962fefce3833d886c655f62 in the kernel.
        clone, clone3, execve, execveat, fork, getpgid, getpgrp, getpid,
        getppid, getrusage, getsid, kill, pidfd_open, pidfd_send_signal, prctl,
        rt_sigqueueinfo, rt_tgsigqueueinfo, setpgid, setsid, tgkill, tkill,
        vfork, wait4, waitid
    }
}

POLICY Resources {
    ALLOW {
        getcpu, getpriority, getrlimit, ioprio_get, sched_getaffinity,
        sched_getattr, sched_getparam, sched_get_priority_max,
        sched_get_priority_min, sched_getscheduler, sched_rr_get_interval
    }
}

// Alter resource settings.
POLICY ResourcesMutation {
    ALLOW {
        ioprio_set, prlimit64, sched_setaffinity, sched_setattr, sched_setparam,
        sched_setscheduler, setpriority, setrlimit
    }
}

POLICY Sandbox {
    ALLOW {
        landlock_add_rule, landlock_create_ruleset, landlock_restrict_self,
        seccomp
    }
}

// Process signal handling.
POLICY Signal {
    ALLOW {
        pause, rt_sigaction, rt_sigpending, rt_sigprocmask, rt_sigreturn,
        rt_sigsuspend, rt_sigtimedwait, sigaltstack, signalfd, signalfd4
    }
}

// Synchronize files and memory to storage.
POLICY Sync {
    ALLOW {
        fdatasync, fsync, msync, sync, sync_file_range, syncfs
    }
}

// Schedule operations by time.
POLICY Timer {
    ALLOW {
        alarm, getitimer, clock_nanosleep, nanosleep, setitimer, timer_create,
        timer_delete, timer_getoverrun, timer_gettime, timer_settime,
        timerfd_create, timerfd_gettime, timerfd_settime
    }
}
)kafel";

class kafel_plugin : emilua::native_module
{
public:
    std::error_code init_lua_module(
        std::shared_lock<std::shared_mutex>& modules_cache_registry_rlock,
        emilua::vm_context& /*vm_ctx*/, lua_State* L) override
    {
        using emilua::push;

        lua_createtable(L, /*narr=*/0, /*nrec=*/2);

        lua_pushliteral(L, "compile");
        lua_pushcfunction(L, compile);
        lua_rawset(L, -3);

        lua_pushliteral(L, "default");
        push(L, DEFAULT_POLICIES);
        lua_rawset(L, -3);

        return {};
    }

private:
    static int compile(lua_State* L)
    {
        using emilua::byte_span_handle;
        using emilua::byte_span_mt_key;
        using emilua::setmetatable;
        using emilua::rawgetp;
        using emilua::push;

        lua_settop(L, 2);

        std::string processed_input;

#define X(S) "#define " BOOST_PP_STRINGIZE(S) " " \
            BOOST_PP_STRINGIZE(__NR_ ## S) "\n"

        {
            static constexpr std::string_view header =
                X(io_pgetevents)
                X(io_uring_enter)
                X(io_uring_register)
                X(io_uring_setup)
                X(futex_waitv)
                X(map_shadow_stack)
                X(rseq)
                X(close_range)
                X(sendfile)
                X(faccessat2)
                X(fstat)
                X(lstat)
                X(openat2)
                X(stat)
                X(statx)
                X(fchmodat2)
                X(epoll_pwait2)
                X(memfd_secret)
                X(pkey_alloc)
                X(pkey_free)
                X(pkey_mprotect)
                X(clone3)
                X(pidfd_open)
                X(pidfd_send_signal)
                X(process_mrelease)
                X(landlock_add_rule)
                X(landlock_create_ruleset)
                X(landlock_restrict_self)
                X(pidfd_getfd)
                X(process_madvise)
                "#define futex_requeue 456\n"
                "#define futex_wait 455\n"
                "#define futex_wake 454\n";

            std::size_t input_len;
            const char* input = luaL_checklstring(L, 1, &input_len);

            processed_input.reserve(header.size() + input_len);
            processed_input.append(header);
            processed_input.append(input, input_len);
        }

#undef X

        kafel_ctxt_t ctxt = kafel_ctxt_create();
        BOOST_SCOPE_EXIT_ALL(&) { kafel_ctxt_destroy(&ctxt); };

        kafel_set_input_string(ctxt, processed_input.c_str());

        switch (lua_type(L, 2)) {
        default:
            push(L, std::errc::invalid_argument, "arg", 2);
            return lua_error(L);
        case LUA_TNIL:
            break;
        case LUA_TTABLE:
            lua_pushliteral(L, "include_path");
            lua_rawget(L, 2);
            switch (lua_type(L, -1)) {
            default:
                push(L, std::errc::invalid_argument, "arg", "include_path");
                return lua_error(L);
            case LUA_TNIL:
                break;
            case LUA_TSTRING:
                kafel_add_include_search_path(ctxt, lua_tostring(L, -1));
                break;
            case LUA_TTABLE:
                for (int i = 1 ;; ++i) {
                    lua_rawgeti(L, -1, i);
                    switch (lua_type(L, -1)) {
                    default:
                        push(L, std::errc::invalid_argument,
                             "arg", "include_path");
                        return lua_error(L);
                    case LUA_TNIL:
                        lua_pop(L, 1);
                        goto end_for;
                    case LUA_TSTRING:
                        kafel_add_include_search_path(
                            ctxt, lua_tostring(L, -1));
                        lua_pop(L, 1);
                        break;
                    }
                }
            end_for:
                break;
            }
            break;
        }

        struct sock_fprog prog;

        if (kafel_compile(ctxt, &prog) != 0) {
            lua_pushstring(L, kafel_error_msg(ctxt));
            return lua_error(L);
        }
        BOOST_SCOPE_EXIT_ALL(&) { free(prog.filter); };

        lua_Integer size = prog.len * sizeof(struct sock_filter);

        auto bs = static_cast<byte_span_handle*>(
            lua_newuserdata(L, sizeof(byte_span_handle))
        );
        rawgetp(L, LUA_REGISTRYINDEX, &byte_span_mt_key);
        setmetatable(L, -2);
        new (bs) byte_span_handle{size, size};

        std::memcpy(bs->data.get(), prog.filter, size);

        return 1;
    }
};

extern "C" BOOST_SYMBOL_EXPORT kafel_plugin EMILUA_PLUGIN_SYMBOL;
kafel_plugin EMILUA_PLUGIN_SYMBOL;
